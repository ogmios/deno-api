# Deno-Api

Deno is a simple, modern and secure runtime for JavaScript and TypeScript that uses V8 and is built in Rust.

[Deno installation](https://deno.land/#installation)

This project contains what is necessary to create an api rest with deno:

Server up,
Logger,
Router,
Controllers,
Middlewares,
JWT

Contains login and CRUD methods.


## Installation

Clone this repo:

```bash
git clone https://gitlab.com/ogmios/deno-api.git
```

And run the following command in the folder project:

```bash
drun
```

drun restart your server when you modify your code. Nodemon style ;)

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)